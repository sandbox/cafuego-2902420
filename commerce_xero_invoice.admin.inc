<?php
/**
 * @file
 * Settings for the commerce_xero_invoice module.
 */

/**
 * Settings form.
 */
function commerce_xero_invoice_admin_settings() {
  $form['commerce_xero_invoice_account'] = array(
    '#type' => 'select',
    '#title' => t('Xero account'),
    '#description' => t('Choose the Xero account type for generated invoices.'),
    '#options' => commerce_xero_revenue_account_options(),
    '#default_value' => variable_get('commerce_xero_invoice_account', NULL),
  );

  $form['commerce_xero_invoice_status'] = array(
    '#type' => 'select',
    '#title' => t('Invoice status'),
    '#description' => t('Choose the default status for new invoices.'),
    '#options' => commerce_xero_invoice_status_options(),
    '#default_value' => variable_get('commerce_xero_invoice_status', 'DRAFT'),
  );

  $form['commerce_xero_invoice_send'] = array(
    '#type' => 'checkbox',
    '#title' => t('Mark as sent'),
    '#description' => t('Check this box to mark the invoice as sent. The invoice status must be set to "Authorised" for this to work.'),
    '#default_value' => variable_get('commerce_xero_invoice_send', 0),
    '#states' => array(
      'visible' => array(
        ':input[name="commerce_xero_invoice_status"]' => array('value' => 'AUTHORISED'),
      ),
    ),
  );

  $form['commerce_xero_invoice_tax'] = array(
    '#type' => 'select',
    '#title' => t('Amounts are'),
    '#description' => t('Choose whether line item amounts include or exclude taxes.'),
    '#options' => commerce_xero_invoice_tax_options(),
    '#default_value' => variable_get('commerce_xero_invoice_tax', 'Exclusive'),
  );

  $form['commerce_xero_invoice_branding_theme'] = array(
    '#type' => 'select',
    '#title' => t('Branding theme'),
    '#description' => t('Choose the branding theme for new invoices.'),
    '#options' => commerce_xero_invoice_branding_theme_options(),
    '#default_value' => variable_get('commerce_xero_invoice_branding_theme', ''),
  );

  return system_settings_form($form);
}
